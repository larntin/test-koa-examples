const router = require("@koa/router")();
const auth = require("basic-auth");
const _ = require("lodash");
const { getFakeList } = require("./fakeList");

router.get("/", async (ctx, next) => {
  await ctx.render("index", {
    title: "Hello Koa 2!",
  });
});

router.get("/api/string", async (ctx, next) => {
  ctx.body = "koa2 string";
});

var uniqueId = null;

function getUniqueId() {
  if (!uniqueId) {
    uniqueId = _.uniqueId("id-");
  }

  return uniqueId;
}

function freshtoken() {
  uniqueId = _.uniqueId("id-");
}

router.post("/api/freshtoken", async (ctx, next) => {
  freshtoken();
  ctx.body = {
    code: 0,
    token: getUniqueId(),
  };
});

// ctx.cookies.set(key,value,[options])
// ctx.cookies.get(key)
// [options]:
// maxAge=> 一个数字表示从Date.now()得到的毫秒值
// expires=> cookie过期时间
// path=> cookie路径，默认‘/’
// domain=> 域名
// secure=> 默认fasle，设置成true则https也可访问
// httpOnly=> 是否只是服务器可访问cookie，默认true

router.post("/api/login", async (ctx, next) => {
  // 第一种：请求头添加 Authorization Basic 的认证方式
  // 请求头添加： Authorization: Basic `base64-Encode(${username}:${password})`
  // a:a => base64编码结果: YTph
  // const user = auth(ctx);
  // if (user.name == "a" && user.pass == "a") {
  //   ctx.cookies.set("token", getUniqueId());
  //   ctx.body = {
  //     code: 0,
  //     msg: "",
  //     token: getUniqueId(),
  //     // 权限
  //     currentAuthority: [],
  //     // 菜单
  //     menus: [],
  //   };
  // } else {
  //   ctx.cookies.set("token", "");
  //   ctx.body = {
  //     code: -1,
  //     msg: "账号密码不匹配",
  //   };
  // }

  // 第二种 post body 放认证数据
  let param = ctx.request.body;
  let doNotCheckPwd = true;
  if (
    doNotCheckPwd ||
    (param["password"] === "a" && param["userName"] === "a")
  ) {
    ctx.cookies.set("token", getUniqueId(), {
      path: "/",
      expires: new Date(Date.now() + 15 * 60 * 1000),
      // maxAge: 15 * 60 * 1000,
      httpOnly: true,
    });
    ctx.body = {
      code: 0,
      msg: "",
      data: [
        {
          id: "10001",
          token: getUniqueId(),
          name: "运营演示账号",
          corp: "无锡城市智慧运营中心",
          station: "前端工程师",
          company: "朗新智诚科技有限公司",
          dept: "研发部",
          currentAuthority: "admin",
        },
      ],
    };
  } else {
    ctx.cookies.set("token", "");
    ctx.body = {
      code: -1,
      msg: "账号密码不匹配",
    };
  }
});

router.post("/api/tokenlogin", async (ctx, next) => {
  if (ctx.request.body["token"] === getUniqueId()) {
    ctx.cookies.set("token", getUniqueId(), {
      path: "/",
      expires: new Date(Date.now() + 15 * 60 * 1000),
      // maxAge: 15 * 60 * 1000,
      httpOnly: true,
    });
    ctx.body = {
      code: 0,
      msg: "",
      data: [
        {
          id: "10001",
          token: getUniqueId(),
          name: "运营演示账号",
          corp: "无锡城市智慧运营中心",
          station: "前端工程师",
          company: "朗新智诚科技有限公司",
          dept: "研发部",
          currentAuthority: "admin",
        },
      ],
    };
  } else {
    ctx.cookies.set("token", "");
    ctx.throw(401, { code: -2, message: "token验证失败！" });
  }
});

router.post("/api/getMenus", async (ctx, next) => {
  let sys = ctx.request.body["sys"] || "app1";
  let menus;
  if (sys === "app1") {
    menus = [
      {
        title: "概览",
        path: "/pdm/welcome",
        icon: "HomeOutlined",
        children: [
          {
            title: "云资源概览",
            path: "/pdm/welcome",
            icon: "WindowsOutlined",
          },
        ],
      },
      {
        title: "List",
        path: "/pdm/list",
        icon: "HomeOutlined",
        children: [
          {
            title: "Basic List",
            path: "/pdm/list/basic-list",
            icon: "WindowsOutlined",
          },
          {
            title: "Card List",
            path: "/pdm/list/card-list",
            icon: "WindowsOutlined",
          },
        ],
      },
    ];
  } else if (sys === "app2") {
    menus = [
      {
        title: "概览",
        path: "/pdm/welcome",
        icon: "HomeOutlined",
        children: [
          {
            title: "云资源概览",
            path: "/pdm/welcome",
            icon: "WindowsOutlined",
          },
        ],
      },
      {
        title: "List",
        path: "/pdm/list",
        icon: "HomeOutlined",
        children: [
          {
            title: "云资源概览",
            path: "/pdm/overview",
            icon: "WindowsOutlined",
          },
        ],
      },
    ];
  }

  ctx.body = {
    code: 0,
    msg: "",
    data: { menus },
  };
});

router.post("/api/logout", async (ctx, next) => {
  ctx.cookies.set("token", "");
  freshtoken();
  ctx.body = {
    code: 0,
    token: getUniqueId(),
  };
});

router.get("/api/currentUser", async (ctx, next) => {
  ctx.body = {
    code: 0,
    msg: "",
    data: [
      {
        id: "10001",
        token: getUniqueId(),
        name: "运营演示账号",
        corp: "无锡城市智慧运营中心",
        station: "前端工程师",
        company: "朗新智诚科技有限公司",
        dept: "研发部",
        currentAuthority: "admin",
        menus: [
          {
            title: "概览",
            path: "/main/folder/over",
            icon: "HomeOutlined",
            children: [
              {
                title: "云资源概览",
                path: "/main/welcome",
                icon: "WindowsOutlined",
              },
            ],
          },
          {
            title: "云资源",
            path: "/main/cloud/reousrce",
            icon: "CloudOutlined",
            children: [
              {
                title: "云资源申请",
                path: "/main/app/catalog/list/basic-list",
                href: "http://localhost:8001/#/pdm/list/basic-list",
                icon: "CloudServerOutlined",
              },
              {
                title: "云资源台账",
                path: "/main/app/catalog/list/card-list",
                href: "http://localhost:8001/#/pdm/list/card-list",
                icon: "FundViewOutlined",
              },
            ],
          },
          {
            title: "系统设置",
            path: "/main/settings",
            icon: "CloudOutlined",
            children: [
              {
                title: "菜单设置",
                path: "/main/settings/menus",
                href: "",
                icon: "CloudServerOutlined",
              },
            ],
          },
        ],
      },
    ],
  };
});

router.get("/api/fake_list", async function (ctx, next) {
  ctx.body = {
    code: 0,
    msg: "",
    data: getFakeList(ctx.request),
  };
});

router.get("/api/notices", async (ctx, next) => {
  ctx.body = {
    code: 0,
    data: [
      {
        id: "000000001",
        avatar:
          "https://gw.alipayobjects.com/zos/rmsportal/ThXAXghbEsBCCSDihZxY.png",
        title: "你收到了 14 份新周报",
        datetime: "2017-08-09",
        type: "notification",
      },
      {
        id: "000000002",
        avatar:
          "https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png",
        title: "你推荐的 曲妮妮 已通过第三轮面试",
        datetime: "2017-08-08",
        type: "notification",
      },
    ],
  };
});

router.get("/api/tags", async function (ctx, next) {
  ctx.body = {
    code: 0,
    data: {
      list: [
        { name: "枣庄市", value: 55, type: 2 },
        { name: "宿迁市", value: 75, type: 1 },
        { name: "离岛", value: 85, type: 1 },
        { name: "长春市", value: 36, type: 1 },
        { name: "白银市", value: 55, type: 1 },
        { name: "黔南布依族苗族自治州", value: 54, type: 0 },
        { name: "莆田市", value: 84, type: 0 },
        { name: "吐鲁番地区", value: 12, type: 2 },
        { name: "邯郸市", value: 52, type: 0 },
        { name: "清远市", value: 82, type: 1 },
        { name: "安康市", value: 55, type: 1 },
        { name: "渭南市", value: 8, type: 2 },
        { name: "扬州市", value: 51, type: 2 },
        { name: "三亚市", value: 49, type: 2 },
        { name: "遵义市", value: 89, type: 0 },
        { name: "淮安市", value: 74, type: 1 },
        { name: "那曲地区", value: 62, type: 2 },
        { name: "吉林市", value: 91, type: 1 },
        { name: "上海市", value: 75, type: 0 },
        { name: "吴忠市", value: 27, type: 2 },
        { name: "延安市", value: 9, type: 1 },
        { name: "临沂市", value: 41, type: 2 },
        { name: "湛江市", value: 44, type: 1 },
        { name: "上海市", value: 50, type: 2 },
        { name: "湘西土家族苗族自治州", value: 58, type: 2 },
        { name: "阳泉市", value: 93, type: 0 },
        { name: "重庆市", value: 36, type: 0 },
        { name: "吴忠市", value: 8, type: 2 },
        { name: "抚州市", value: 26, type: 1 },
        { name: "佳木斯市", value: 23, type: 2 },
        { name: "九龙", value: 92, type: 1 },
        { name: "湘潭市", value: 64, type: 1 },
        { name: "武威市", value: 92, type: 0 },
        { name: "长春市", value: 6, type: 1 },
        { name: "花莲县", value: 24, type: 2 },
        { name: "重庆市", value: 62, type: 2 },
        { name: "台南市", value: 42, type: 1 },
        { name: "兰州市", value: 28, type: 2 },
        { name: "吴忠市", value: 57, type: 2 },
        { name: "齐齐哈尔市", value: 6, type: 1 },
        { name: "信阳市", value: 82, type: 1 },
        { name: "上海市", value: 60, type: 1 },
        { name: "衢州市", value: 81, type: 1 },
        { name: "本溪市", value: 85, type: 1 },
        { name: "铜川市", value: 44, type: 0 },
        { name: "德宏傣族景颇族自治州", value: 91, type: 1 },
        { name: "延安市", value: 18, type: 2 },
        { name: "漳州市", value: 89, type: 1 },
        { name: "枣庄市", value: 63, type: 1 },
        { name: "贵港市", value: 30, type: 2 },
        { name: "日喀则地区", value: 31, type: 1 },
        { name: "南通市", value: 64, type: 1 },
        { name: "九江市", value: 96, type: 1 },
        { name: "北海市", value: 71, type: 1 },
        { name: "邢台市", value: 35, type: 1 },
        { name: "晋城市", value: 99, type: 1 },
        { name: "本溪市", value: 45, type: 1 },
        { name: "三明市", value: 60, type: 0 },
        { name: "北京市", value: 18, type: 0 },
        { name: "林芝地区", value: 90, type: 2 },
        { name: "安阳市", value: 70, type: 1 },
        { name: "白城市", value: 68, type: 2 },
        { name: "威海市", value: 86, type: 1 },
        { name: "廊坊市", value: 14, type: 0 },
        { name: "乐山市", value: 43, type: 1 },
        { name: "常德市", value: 47, type: 0 },
        { name: "咸宁市", value: 85, type: 1 },
        { name: "南昌市", value: 91, type: 0 },
        { name: "朔州市", value: 63, type: 1 },
        { name: "宜春市", value: 82, type: 1 },
        { name: "重庆市", value: 81, type: 2 },
        { name: "海外", value: 56, type: 1 },
        { name: "松原市", value: 27, type: 1 },
        { name: "长治市", value: 88, type: 2 },
        { name: "三沙市", value: 54, type: 0 },
        { name: "淮南市", value: 5, type: 0 },
        { name: "聊城市", value: 99, type: 1 },
        { name: "北京市", value: 43, type: 1 },
        { name: "固原市", value: 82, type: 1 },
        { name: "巴彦淖尔市", value: 39, type: 0 },
        { name: "嘉峪关市", value: 38, type: 1 },
        { name: "铁岭市", value: 88, type: 0 },
        { name: "普洱市", value: 27, type: 2 },
        { name: "漳州市", value: 19, type: 1 },
        { name: "天津市", value: 84, type: 2 },
        { name: "楚雄彝族自治州", value: 40, type: 1 },
        { name: "镇江市", value: 53, type: 1 },
        { name: "聊城市", value: 61, type: 1 },
        { name: "澳门半岛", value: 3, type: 1 },
        { name: "澳门半岛", value: 24, type: 1 },
        { name: "临汾市", value: 91, type: 1 },
        { name: "常德市", value: 80, type: 1 },
        { name: "鄂尔多斯市", value: 78, type: 0 },
        { name: "杭州市", value: 1, type: 1 },
        { name: "新余市", value: 30, type: 1 },
        { name: "连江县", value: 98, type: 0 },
        { name: "北京市", value: 3, type: 0 },
        { name: "海外", value: 86, type: 1 },
        { name: "黔东南苗族侗族自治州", value: 84, type: 0 },
        { name: "茂名市", value: 64, type: 2 },
      ],
    },
  };
});

module.exports = {
  routers: router,
  getUniqueId,
};
