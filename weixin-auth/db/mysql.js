var mysql = require("mysql");

let host = "172.22.3.114";
let user = "root";
let password = "longshine01";
let database = "yazhou-mp";

var pool = mysql.createPool({
  connectionLimit: 10,
  host: host,
  user: user,
  password: password,
  database: database,
});

// 第一种方式：使用 pool.query ，是代码流的快捷方式
// pool.getConnection() -> connection.query() -> connection.release()
// pool.query("SELECT 1 + 1 AS solution", function (error, results, fields) {
//   if (error) throw error;
//   console.log("The solution is: ", results[0].solution);
// });

// 第二种：需要自己手动释放

// pool.getConnection(function(err, connection) {
//   if (err) throw err; // not connected!

//   // Use the connection
//   connection.query('SELECT something FROM sometable', function (error, results, fields) {
//     // When done with the connection, release it.
//     connection.release();

//     // Handle error after the release.
//     if (error) throw error;

//     // Don't use the connection here, it has been returned to the pool.
//   });
// });

// 第一个参数是错误
// connection.connect(function(err) {
//   console.log(err.code); // 'ECONNREFUSED'
//   console.log(err.fatal); // true
// });

// connection.query('SELECT 1', function (error, results, fields) {
//   console.log(error.code); // 'ECONNREFUSED'
//   console.log(error.fatal); // true
// });

module.exports = { pool };
