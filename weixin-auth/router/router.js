const router = require("@koa/router")({
  prefix: "/api",
});
const auth = require("basic-auth");
const axios = require("axios");
const _ = require("lodash");

// auth.code2Session
// GET https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code
// 属性	类型	说明
// openid	string	用户唯一标识
// session_key	string	会话密钥
// unionid	string	用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明。
// errcode	number	错误码
//   ├─ -1	系统繁忙，此时请开发者稍候再试
//   ├─ 0	请求成功
//   ├─ 40029	code 无效
//   └─ 45011	频率限制，每个用户每分钟100次
// errmsg	string	错误信息

const APPID = "wx36fdb2c788a0fc53";
const SECRET = "ff26bd969af50f5944c14b57140317b8";
// 授权类型，此处只需填写 authorization_code
const grant_type = "authorization_code";

async function authCode2Session(jscode) {
  let url = `https://api.weixin.qq.com/sns/jscode2session?appid=${APPID}&secret=${SECRET}&js_code=${jscode}&grant_type=${grant_type}`;
  return axios.get(url).then(({ data }) => {
    if (data.errcode && data.errcode !== 0)
      return [{ errcode: data.errcode, errmsg: data.errmsg }, undefined];
    else
      return [
        undefined,
        {
          openid: data.openid,
          session_key: data.session_key,
          unionid: data.unionid,
        },
      ];
  });
}

router.post("/mp-login", async (ctx, next) => {
  const body = ctx.request.body;
  const { jscode } = body;
  let [err, data] = await authCode2Session(jscode);
  if (err) {
    return (ctx.body = {
      code: err["errcode"],
      msg: err["errmsg"],
    });
  } else {
    // session_key 保留
    console.log(`authCode2Session=${JSON.stringify(data)}`);
    let { session_key, ...otherdata } = data;
    return (ctx.body = {
      code: "0",
      data: otherdata,
      msg: "success",
    });
  }
});

router.get("/mp/hello", (ctx, next) => {
  ctx.body = {
    code: "0",
    data: { msg: "hello - testaaa" },
    msg: "success",
  };
});

module.exports = {
  routers: router,
};
