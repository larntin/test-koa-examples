const Koa = require("koa");
const app = (module.exports = new Koa());
const views = require("koa-views");
const logger = require('koa-logger');
const static = require('koa-static');
const koaBody = require('koa-body');
const cors = require('koa2-cors');

const routers = require("./router/router");

app.use(static(__dirname + './static/'))

app.use(koaBody({
  jsonLimit: '1kb'
}));

// 在使用之前设置好模板引擎
app.use(
  views(__dirname + "/views", {
    extension: "ejs",
  })
);

// cors()使用样例
// app.use(cors({
//   origin: function (ctx) {
//       if (ctx.url === '/cors') {
//           return "*"; // 允许来自所有域名请求
//       }
//       return 'http://localhost:3201';
//   },
//   exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
//   maxAge: 7200,
//   credentials: true,
//   allowMethods: ['GET', 'POST', 'DELETE'], //设置允许的HTTP请求类型
//   allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
// }));

app.use(cors());

// 添加日志
app.use(logger());

app.use(routers.routers.routes());

// 在 115 上的 9004 端口
if (!module.parent) app.listen(9004);
