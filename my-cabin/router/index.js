const router = require("@koa/router")({ prefix: "/api/cabin" });
const _ = require("lodash");

// 翻牌器
router.get("/flop", async (ctx, next) => {
  ctx.body = {
    value: _.random(1000, 2000),
  };
});

// 饼图
router.get("/pie", async (ctx, next) => {
  let _q = ctx.request.query;
  const data1 = [
    {
      name: "苹果",
      value: 1000879,
    },
    {
      name: "三星",
      value: 3400879,
    },
    {
      name: "小米",
      value: 2300879,
    },
  ];
  const data2 = [
    {
      name: "oppo",
      value: 5400879,
    },
    {
      name: "大疆",
      value: 3000,
    },
    {
      name: "抖音",
      value: 2000,
    },
  ];
  if (_q.a === "1") ctx.body = data1;
  else ctx.body = data2;
});

// 柱图
router.get("/bar", async (ctx, next) => {
  let _q = ctx.request.query;
  const data1 = {
    categories: ["苹果", "三星"],
    series: [
      {
        name: "手机品牌",
        data: [1000879, 3400879],
      },
    ],
  };
  const data2 = {
    categories: ["小米", "oppo", "vivo"],
    series: [
      {
        name: "手机品牌",
        data: [2300879, 5400879, 3400879],
      },
    ],
  };
  if (_q.a === "1") ctx.body = data1;
  else ctx.body = data2;
});

// gauge
router.get("/gauge", async (ctx, next) => {
  let _q = ctx.request.query;
  const data1 = {
    min: 1,
    max: 100,
    label: "名称1",
    value: 95,
    unit: "%",
  };
  const data2 = {
    min: 1,
    max: 100,
    label: "名称2",
    value: 20,
    unit: "%",
  };
  if (_q.a === "1") ctx.body = data1;
  else ctx.body = data2;
});

module.exports = {
  routers: router,
};
