const Koa = require("koa");
const koaBody = require("koa-body");
const app = (module.exports = new Koa());
const views = require("koa-views");
const logger = require("koa-logger");

const routers = require("./router/index");

// 在使用之前设置好模板引擎
app.use(
  views(__dirname + "/views", {
    extension: "ejs",
  })
);

// 添加日志
app.use(logger());

app.use(koaBody());

// 标准的过滤器写法
// app.use(async function (ctx, next) {
//   await next();
// });

app.use(routers.routers.routes());

if (!module.parent) app.listen(3002);
